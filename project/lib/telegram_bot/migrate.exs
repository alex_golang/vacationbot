defmodule RelaxTelegramBot.Migrator do
  use Ecto.Migration

  def run_migrations do
    {:ok, repo} =
      Ecto.Repo.Supervisor.start_link(
        RelaxTelegramBot.Repo,
        RelaxTelegramBot.Repo.init()
      )

    # Путь
    migrations_path = RelaxTelegramBot.app_dir(:relax_telegram_bot, "priv/repo/migrations")

    Ecto.Migrator.run(repo, RelaxTelegramBot.Repo.Migrations, :up, migrations_path)
  end
end


RelaxTelegramBot.Migrator.run_migrations()